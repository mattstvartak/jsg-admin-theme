<?php
/**
 * JumpShot Genie® Admin Theme
 *
 * @package   JSG_Admin_Theme
 * @author    Matt Stvartak <matt@theideapeople.com>
 * @license   GPL-2.0+
 * @link      http://www.theideapeople.com
 * @copyright 2013 © The Idea People
 */

/**
 * JumpShot Genie® Admin Theme Class
 *
 * @package JSG_Admin_Theme
 * @author  Matt Stvartak <matt@theideapeople.com>
 */
class JSGAdminTheme {

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since     1.0.0
	 */
	public function __construct() {
		add_action( 'wp_before_admin_bar_render', array(&$this, 'remove_toolbar_menus' ) );
		add_action( 'admin_init', array(&$this, 'remove_admin_styles' ) );
		add_action( 'wp_before_admin_bar_render', array(&$this, 'add_nodes' ) );
		add_action( 'admin_enqueue_scripts', array(&$this, 'enqueue_scripts_and_styles' ) );
		add_action( 'admin_footer', array(&$this, 'add_custom_html' ) );
		add_filter( 'body_class', array(&$this, 'hide_wpbody') );
	}

	/**
	 * Removes unwanted items from the Wordpress Admin Toolbar.
	 *
	 * @since  1.0.0
	 */
	public function remove_toolbar_menus() {
		global $wp_admin_bar;

		$wp_admin_bar->remove_menu( 'wp-logo' );
		$wp_admin_bar->remove_menu( 'site-name' );
		$wp_admin_bar->remove_menu( 'comments' );
		$wp_admin_bar->remove_menu( 'new-content' );
		$wp_admin_bar->remove_menu( 'my-account' );
		$wp_admin_bar->remove_menu( 'updates' );
	}

	/**
	 * Removes the default Wordpress Admin styles on Admin Init
	 *
	 * @since  1.0.0
	 */
	public function remove_admin_styles() {
		wp_deregister_style('admin-bar');
		wp_deregister_style('wp-admin');
	}

	/**
	 * Adds new top level menu item to the admin toolbar
	 *
	 * @since 1.0.0
	 *
	 * @param int $id The node ID.
	 * @param string $title The node title.
	 * @param boolean $parent Does the node have a parent?
	 * @param string $href The node link.
	 * @param boolean group Is the node a group node?
	 * @param array $meta The node meta information.
	 *
	 * @return void
	 **/
	public function create_node( $id, $title, $parent = FALSE, $href = FALSE, $group = FALSE, $meta = array() ) {
		global $wp_admin_bar;

		if ( !is_super_admin() || !is_admin_bar_showing() )
			return;

		$wp_admin_bar->add_node( array(
			'id' => $id,
			'title' => $title,
			'parent' => $parent,
			'href' => $href,
			'group' => $group,
			'meta' => $meta

		) );
	}

	/**
	 * Defines the nodes to be created in the admin bar
	 *
	 * @since  1.0.0
	 **/
	public function add_nodes() {
		global $current_user;
		get_currentuserinfo();
		$user_data = get_userdata( $current_user->ID );

		// Adds logo
		$this->create_node( 'jsg-logo', '<h1><strong>Jumpshot</strong>Genie&reg;</h1>', '', get_bloginfo( 'site_url' ) );

		// Adds logout button
		$this->create_node( 'user-logout', '<span class="icon-logout"></span>', '', wp_logout_url( admin_url() ), '');

		// Adds user dropdown menu
		$this->create_node( 'user-menu', '<a href="' .  get_edit_user_link( $current_user->ID) . '" target="_self"><div class="user-image">' . get_avatar($current_user->ID, 36) . '</div><div class="user-greeting">Welcome back, ' . $current_user->user_firstname . ' ' . $current_user->user_lastname . '</div></a>', '' , '', '' );

		// Adds user profile picture to sub menu
		$this->create_node( 'user-avatar', '<div class="user-image">' . get_avatar($current_user->ID, 54) . '</div><div class="user-name">' . $current_user->user_firstname . ' ' . $current_user->user_lastname . '<br><span class="user-role">' . $user_data->roles[0] . '</span></div>', 'user-menu', '', '');

		// Adds edit profile link
		$this->create_node( 'user-edit-profile', '<span class="icon-gear"></span>Edit My Profile', 'user-menu', get_edit_user_link($current_user->ID), '');
		// Adds support link
		$this->create_node( 'user-support', '<span class="icon-help"></span>Support', 'user-menu', get_edit_user_link($current_user->ID), '');

		// Placeholder for messages alert
		$this->create_node( 'user-messages', '<span class="icon-mail"></span>', '', '', '' );

		// Placeholder for alters
		$this->create_node( 'user-alerts', '<span class="icon-alert"></span>', '', '', '' );
	}

	/**
	 * Get the admin theme styles and scripts
	 */
	public function enqueue_scripts_and_styles() {
		// Stykes
		wp_register_style( 'jsg-admin', JSGADMINTHEME_URL . '/assets/css/jsg-admin.css', '', '', 'all' );

		//Enqueue styles
		wp_enqueue_style( 'jsg-admin' );

		// Scripts
		wp_register_script( 'jsg-admin-script', JSGADMINTHEME_URL . '/assets/js/main.js', 'jquery' );

		// Enqueue scripts
		wp_enqueue_script( 'jsg-admin-script' );
	}

	/**
	 * Adds the custom HTML needed for the admin theme before the Wordpress footer is rendered
	 *
	 * @return void
	 */
	public function add_custom_html() {
		// Gets the curren admin screen and prints it above the sidebar
		$current_page = get_current_screen();

		if ( $current_page->post_type ) {
			$page_title = $current_page->post_type;
		} else {
			$page_title = $current_page->id;
		}

		echo '<div id="jsg-page-title"><h2></h2></div>';
	}

	public function hide_wpbody() {
		return 'hidden';
	}
}
?>