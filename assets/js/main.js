(function($) {
	$(document).ready(function() {
		var adminMenuLinks = $('#adminmenu > li > a');

		/**
		 * Set the page title of the current active page.
		 */
		function activeMenuTitle() {
			var pageTitle = $('#jsg-page-title h2'),
				activePage = $('#adminmenu > li > a[class*="current"]:not([class*="not-current"]) .wp-menu-name').html();

			pageTitle.html(activePage);
		}
		activeMenuTitle();

		/**
		 * Page transitions
		 */
		adminMenuLinks.bind('click', function(e) {
			var link = $(this),
				newLink = $(this).add( $(this).parent('li') ),
				currentLink = $('li.wp-menu-open, a.wp-menu-open'),
				currentSubmenu = $('li.wp-menu-open .wp-submenu'),
				newSubmenu = $(this).siblings('.wp-submenu');

			// Prevent default link action
			e.preventDefault();

			if ( currentLink.hasClass('wp-has-submenu') && newLink.hasClass('wp-has-submenu') ) {
				// Slide up/down new and old menus.
				newSubmenu.add(currentSubmenu).slideToggle(250).promise().done(function() {
					// Toggle the class the effects the bottom border and arrow
					newSubmenu.add(newLink).add(currentSubmenu).add(currentLink).toggleClass('wp-has-current-submenu wp-not-current-submenu');
				});

				// Toggle the active menu selector
				newSubmenu.add(newLink).add(currentSubmenu).add(currentLink).add(link).toggleClass('wp-menu-open');
			} else if ( currentLink.hasClass('wp-has-submenu') ) {
				// Slide up/down old menus.
				currentSubmenu.slideToggle(250).promise().done(function() {
					// Toggle the class the effects the bottom border and arrow
					currentSubmenu.add(currentLink).toggleClass('wp-has-current-submenu wp-not-current-submenu');

					$('#wpbody').toggleClass('show');
					// Redirect to new page
					setTimeout( function() {
						window.location = link.attr('href');
					}, 250);

				});

				// Toggle the active menu selector
				newLink.add(link).add(currentLink).toggleClass('wp-menu-open');
			} else if ( newLink.hasClass('wp-has-submenu') ) {
				// Slide up/down new menus.
				newSubmenu.slideToggle(250).promise().done(function() {
					// Toggle the class the effects the bottom border and arrow
					newSubmenu.add(newLink).add(currentSubmenu).add(currentLink).toggleClass('wp-has-current-submenu wp-not-current-submenu');
				});

				// Toggle the active menu selector
				newSubmenu.add(newLink).add(currentLink).add(link).toggleClass('wp-menu-open');
			} else {
				// Redirect to new page
				$('#wpbody').toggleClass('show');
				setTimeout( function() {
					window.location = link.attr('href');
				}, 250);
			}
		});
		/**
		 * Adjust wpbody height
		 */
		var content = $('#wpcontent'),
			windowHeight = $(window).height(),
			contentHeight = content.height();

		$(window).bind('load resize', function() {
			windowHeight = $(window).height();

			if ( windowHeight > contentHeight ) {
				content.height(windowHeight);
			}
		});

		/**
		 * Show content on page load
		 */
		setTimeout(function() {
			$('#wpbody').toggleClass('show');
		}, 500);
		$('a').not('#adminmenu > li > a').click(function() {
			var link = $(this).attr('href');
			// Redirect to new page
			$('#wpbody').toggleClass('show');
			setTimeout( function() {
				window.location = link;
			}, 250);
		});
	});
})(jQuery);